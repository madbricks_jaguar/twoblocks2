﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowLock : MonoBehaviour
{
    private DoTweenController animationControl;
    public LevelManager levelManger;
    Vector3 scaleOfsset = Vector3.zero;
    private void Start()
    {
        animationControl = GetComponent<DoTweenController>();

        Invoke("SetMyPosition",0.01f);
    }

    public void SetMyPosition()
    {
        scaleOfsset.z = transform.localScale.y / 2;
        transform.position = levelManger.levelObjects[levelManger.splitLevels].transform.position + scaleOfsset + Vector3.up *4;
    }

    public void ActiveAnimation()
    {
        animationControl.originalPosition = transform.position;
        animationControl.targetPosition = levelManger.levelObjects[levelManger.splitLevels].transform.position + scaleOfsset - Vector3.up * 10;
        animationControl.CheckAnimation();
    }
}
