﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridNodes : MonoBehaviour
{
    public NodeContainer[] nodes;
    [HideInInspector]
    public List<AgentPathFinding> agentsWaiting = new List<AgentPathFinding>();
    [HideInInspector]
    public List<Node> thePath = new List<Node>();

    [SerializeField] private EnemysController enemyController;

    private bool calculatingPath;
    [Range(0.3f,2)]
    public float enemiesIntervalsTime = 0.4f;


    private void Start()
    {

        for (int i = 0; i < enemyController.enemys.Length; i++)
        {
          agentsWaiting.Add(enemyController.enemys[i].GetComponent<AgentPathFinding>());
        }

        StartCoroutine(StartSearchOfEnemies());
    }

    public void SetGridNodes(NodeContainer[] _nodes)
    {
        nodes = _nodes;

        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].grid = this;
        }
    }

    public List<Node> GetPath()
    {
        return thePath;
    }

    public void RemovePath()
    {
        foreach (Node _node in thePath)
        {
            _node.RestoreNode();
        }
    }

    public void CallNextEnemy(AgentPathFinding agent)
    {
        agent.ReccorerCamino();
        RemovePath();
        thePath.RemoveRange(0, thePath.Count);
        calculatingPath = false;
    }

    public IEnumerator StartSearchOfEnemies()
    {
        if (agentsWaiting.Count == 0) { yield break; }

        foreach (AgentPathFinding agent in agentsWaiting)
        {
            if (agent.gameObject.activeInHierarchy)
            {
                if (agent.canISearch && !calculatingPath)
                {
                    agent.StartSearch();
                    calculatingPath = true;
                }
            }

            yield return new WaitForSeconds(enemiesIntervalsTime);

        }
       StartCoroutine(StartSearchOfEnemies());
    }

    public IEnumerator CalculateEnemyMovement(AgentPathFinding agente)
    {
        yield return new WaitForSeconds(enemiesIntervalsTime);
        agente.GetPathFromGrid();
    }
}
