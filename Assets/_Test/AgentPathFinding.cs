﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgentPathFinding : MonoBehaviour
{

 //   [HideInInspector]
    public NodeContainer TestCurrentNode;
  //  [HideInInspector]
    public NodeContainer TestFinalNode;

    [SerializeField] private GridNodes grid;

  //  [HideInInspector]
    public Node currentNode;
    private Node firstNode;
    private Node finalNode;
  //  [HideInInspector]
    public List<Node> nodesGo = new List<Node>();
  // [HideInInspector]
    public List<Node> correctNodes = new List<Node>();
    public bool canISearch = true;
    private AIBehaviour AI;


    private void Start()
    {
        AI = GetComponent<AIBehaviour>();
    }

    public void StartSearch()
    {
        if (canISearch)
        {
            AI = GetComponent<AIBehaviour>();

            TestCurrentNode = grid.GetComponent<GridManager>().FindPositions(transform.position).GetComponent<NodeContainer>();
            TestFinalNode = AI.GetTarget();

            currentNode = TestCurrentNode.node;
            firstNode = currentNode;
            finalNode = TestFinalNode.node;


            StartCoroutine(currentNode.owner.Compile(0, finalNode.owner));
            StartCoroutine(grid.CalculateEnemyMovement(this));
            canISearch = false;
        }
    }

    public void GetPathFromGrid()
    {
        nodesGo = grid.GetPath();
        StartCoroutine(CheckPath());
    }

    public IEnumerator CheckPath()
    { 
        Node presentNode = finalNode;
        Node nextNode  = presentNode;
        int lessWeigt = 100000;

        while (!ReferenceEquals(presentNode,firstNode))
        {
            if (TestCurrentNode == null && TestFinalNode)
            {
                yield break;
            }

            if (presentNode.backNeighbor != null && presentNode.backNeighbor.calculated == true)
            {
                if (presentNode.backNeighbor.weight < lessWeigt)
                {
                    lessWeigt = presentNode.backNeighbor.weight;
                    nextNode = presentNode.backNeighbor;
                }
            }

            if (presentNode.frontNeighbor != null && presentNode.frontNeighbor.calculated == true)
            {
                if (presentNode.frontNeighbor.weight < lessWeigt)
                {
                    lessWeigt = presentNode.frontNeighbor.weight;
                    nextNode = presentNode.frontNeighbor;
                }
            }
            if (presentNode.rightNeighbor != null && presentNode.rightNeighbor.calculated == true)
            {
                if (presentNode.rightNeighbor.weight < lessWeigt)
                {
                    lessWeigt = presentNode.rightNeighbor.weight;
                    nextNode = presentNode.rightNeighbor;
                }
            }
            if (presentNode.leftNeighbor != null && presentNode.leftNeighbor.calculated == true)
            {
                if (presentNode.leftNeighbor.weight < lessWeigt)
                {
                    lessWeigt = presentNode.leftNeighbor.weight;
                    nextNode = presentNode.leftNeighbor;
                }
            }

            presentNode = nextNode;

            if (!ReferenceEquals(presentNode, firstNode))
            {
                 correctNodes.Add(nextNode);
            }

            if (canISearch) { yield break;}

            yield return new WaitForSeconds(0.01f);
        }
        canISearch = true;
        grid.CallNextEnemy(this);

    }


    public void ReccorerCamino()
    {
        if (TestFinalNode == null)
        {
            return;
        }

        if (correctNodes.Count > 0)
        {
            StartCoroutine(AI.MoveEnemy(correctNodes[correctNodes.Count - 1].position));
            TestCurrentNode = correctNodes[correctNodes.Count - 1].owner;
        }
        else
        {
            if (TestFinalNode!=null)
            {
               StartCoroutine(AI.MoveEnemy(finalNode.position));
                TestCurrentNode = finalNode.owner;

            }
        }

        correctNodes.RemoveRange(0, correctNodes.Count);

        TestFinalNode = null;
        TestCurrentNode = null;

    }
}
