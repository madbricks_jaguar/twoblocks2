﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class NodeContainer : MonoBehaviour
{
    public TextMeshProUGUI text;
    //[HideInInspector]
    public Node node;
    [HideInInspector]
    public GridNodes grid;


    private void Awake()
    {
        node = new Node(0,false,transform.position,this);
    }
    public void SetWeight(int w)
    {
        node.calculated = true;
        node.weight = w;
       // text.text =""+w;
    }

    public IEnumerator Compile(int weight, NodeContainer target)
    {
        yield return new WaitForSeconds(0.01f);

        if (!ReferenceEquals(target, this))
        {
            SetWeight(weight++);

            if (!grid.thePath.Contains(node))
            {
                grid.thePath.Add(node);
            }

            if (node.backNeighbor != null && node.backNeighbor.calculated == false)
            {
                StartCoroutine(node.backNeighbor.owner.Compile(weight, target));
            }

            if (node.frontNeighbor != null && node.frontNeighbor.calculated == false)
            {
                StartCoroutine(node.frontNeighbor.owner.Compile(weight, target));
            }

            if (node.rightNeighbor != null && node.rightNeighbor.calculated == false)
            {
                StartCoroutine(node.rightNeighbor.owner.Compile(weight, target));
            }

            if (node.leftNeighbor != null && node.leftNeighbor.calculated == false)
            {
                StartCoroutine(node.leftNeighbor.owner.Compile(weight, target));
            }
        }
        else
        {
        //  text.text = "y";
        }
    }

}

[System.Serializable]
public class Node
{
    public int weight;
    public bool calculated;
    public Node rightNeighbor;
    public Node leftNeighbor;
    public Node backNeighbor;
    public Node frontNeighbor;
    public NodeContainer owner;
    public Vector3 position;

    public Node(int _weight, bool _calculated,Vector3 pos, NodeContainer _owner)
    {
        weight = _weight;
        calculated = _calculated;
        position = pos;
        owner = _owner;
    }
    public Vector3 AlignPosition(Vector3 from)
    {
        if (from.x < position.x + 0.5f && from.x > position.x - 0.5f && from.z < position.z + 0.5f && from.z > position.z - 0.5f)
        {
            return position;
        }

        return from;
    }

    public void RestoreNode()
    {
        calculated = false;
        weight = 0;
    }
}
