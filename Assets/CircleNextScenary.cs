﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class CircleNextScenary : MonoBehaviour
{

    [SerializeField] private CameraSwipeMove cameraSwipe;
    [SerializeField] private LevelManager levelManager;
    [SerializeField] private TextMeshProUGUI textLevelReached;
    [SerializeField] private ShadowLock shadow;
    public UnityEvent circleCompletedCallBack;
    private Image circleImage;
    private void Start()
    {
        circleImage = GetComponent<Image>();
        cameraSwipe.OnLimited += FillCircle;

    }

    private void FillCircle(float _fillAmount)
    {
        if (levelManager.splitLevels != (PlayerPrefs.GetInt(KeysStorage.LEVEL)))
        {
            if (_fillAmount > 0f)
            {
                textLevelReached.gameObject.SetActive(true);
            }

            return;
        }

        circleImage.fillAmount = _fillAmount;

        if (_fillAmount >= 1)
        {
            circleCompletedCallBack?.Invoke();
            shadow.ActiveAnimation();
            circleImage.fillAmount = 0;
        }
    }
}
