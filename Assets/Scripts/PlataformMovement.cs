﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlataformMovement : MonoBehaviour
{

    public DoTweenController controller;
    private Vector3 nodePositionOrigin;
    private Vector3 nodePositionFinal;
    private NodeContainer containerNode;

    private void Start()
    {
        containerNode = GetComponent<NodeContainer>();
        nodePositionOrigin = containerNode.node.position;
    }


    private void Update()
    {
        containerNode.node.position = transform.position;

    }

    public void SwitchMovement()
    {
        if ((int)transform.position.x == (int)controller.originalLocalPosition.x && (int)transform.position.z == (int)controller.originalLocalPosition.z)
        {
            nodePositionFinal = controller.targetPosition;
            controller._animationType = DoTweenController.AnimationType.localPosition;
            controller.CheckAnimation();
           // containerNode.node.position = nodePositionFinal;
        }
        else if((int)transform.position.x == (int)controller.targetPosition.x && (int)transform.position.z == (int)controller.targetPosition.z)
        {
            controller._animationType = DoTweenController.AnimationType.localReturnPosition;
            controller.CheckAnimation();
          //  containerNode.node.position = nodePositionOrigin;
        }
    }
}
