﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RestartLevel : MonoBehaviour
{
    public LayerMask detectionLayers;
    private ScoringSystem scoreSyste;
    private bool somethingFell;

    public void Awake()
    {
        scoreSyste = GetComponent<ScoringSystem>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(!somethingFell && detectionLayers == (detectionLayers | (1 << other.gameObject.layer))) 
        {
            LoseByFall();
        }
    }

    public void LoseByFall()
    {
        if (scoreSyste.SubstractLife())
        {
            GameManager.instance.StartCoroutine(GameManager.instance.BackFirstSublevel());
        }
        else
        {
            Restart();
        }
        somethingFell = true;
    }

    void Restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        scoreSyste.AddStep();

    }
}
