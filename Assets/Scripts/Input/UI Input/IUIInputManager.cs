﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


namespace UIInput{
	public interface IUIInputManager {

		event Action<Vector3> SwipeUsed;
		event Action<Vector3> Tap;
		event Action DragStart;
		//Drag Vector, DragDeltaThisFrame
		event Action<Vector3, Vector3> DragUsed;
		event Action<Vector3> DragEnd;
	}
}