﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using Zenject;
using System;


namespace UIInput.Detail{
	public class UIInputManager : MonoBehaviour, IUIInputManager {

		public event Action<Vector3> SwipeUsed;
		public event Action<Vector3> Tap;
		public event Action DragStart;
		public event Action<Vector3, Vector3> DragUsed;
		public event Action<Vector3> DragEnd;

		//[Inject]
		private UITouchInput touchInput;

       // [Inject]
        private void Inject()
		{
			touchInput.SwipeUsed += RaiseSwipeUsed;
			touchInput.Tap += RaiseTap;
			touchInput.DragStart += RaiseDragStart;
			touchInput.DragUsed += RaiseDrag;
			touchInput.DragEnd += RaiseDragEnd;
		}

		private void Start()
		{
			touchInput.Switch ();
		}

		private void Update()
		{
			touchInput.Update ();
		}

		private void RaiseSwipeUsed(Vector3 Swipe)
		{
			if (SwipeUsed != null)
				SwipeUsed (Swipe);
		}

		private void RaiseTap(Vector3 position)
		{
			if (Tap != null)
				Tap (position);
		}

		private void RaiseDragStart()
		{
			if (DragStart != null)
				DragStart ();
		}

		private void RaiseDrag(Vector3 drag, Vector3 deltaDrag)
		{
			if (DragUsed != null)
				DragUsed (drag, deltaDrag);
		}

		private void RaiseDragEnd(Vector3 deltaDrag)
		{
			if (DragEnd != null)
				DragEnd (deltaDrag);
		}
	}
}
