﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameInput;
using System;
using GameInput.Detail;

namespace UIInput.Detail{
	[Serializable]
	public class UITouchInput : TouchInput
    {
        public event Action<Vector3> SwipeUsed;
		public event Action<Vector3> Tap;
		public event Action DragStart;
		public event Action<Vector3, Vector3> DragUsed;
		public event Action<Vector3> DragEnd;

		private Vector3 deltaDragLastFrame;

		public override void Update()
		{
			if (IsEnable)
			{
				if (PrimaryTouch.StartedTouchThisFrame)
					RaiseTouchStarted();

				if (PrimaryTouch.ReleasedTouchThisFrame)
					CheckSwipe();

				if (PrimaryTouch.ReleasedTapThisFrame)
					RaiseTap (); 
				if (PrimaryTouch.StartedDragThisFrame)
					RaiseDragStart ();
				if (PrimaryTouch.IsDragging)
					RaiseDrag ();
				if (PrimaryTouch.ReleasedDragThisFrame)
					RaiseDragEnd ();
			}
		}

		protected override void CheckSwipe()
		{
			RaiseTouchEnded();
			bool displacement = PrimaryTouch.DragVector.magnitude > MinDisplacement;
			bool time = Time.time - PrimaryTouch.StartTouchTime <= maxSwipeTime;
			if (displacement && time)
				RaiseSwipe (PrimaryTouch.DragVector);
		}

		private void RaiseSwipe(Vector2 swipe)
		{
			Vector3 worldSwipe = GetCameraRelativeDrag (swipe);
			if (SwipeUsed != null)
				SwipeUsed (worldSwipe);
		}

		private void RaiseTap()
		{
			if (Tap != null)
				Tap (PrimaryTouch.PreviousTouchPosition);
		}

		private void RaiseDragStart()
		{
			if (DragStart != null)
				DragStart ();
		}

		private void RaiseDrag()
		{
			if(DragUsed!= null)
				DragUsed(PrimaryTouch.DragVector, PrimaryTouch.DragDeltaThisFrame);
			deltaDragLastFrame = PrimaryTouch.DragDeltaThisFrame;
		}

		private void RaiseDragEnd()
		{
			if (DragEnd != null)
				DragEnd (deltaDragLastFrame);
			deltaDragLastFrame = Vector3.zero;
		}
	}
}
