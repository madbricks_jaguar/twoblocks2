﻿using System;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public class KeyboardInput : BaseInput
    {
        private const string northButtonName = "North";
		private const string eastButtonName = "East";
		private const string westButtonName = "West";
		private const string southButtonName = "South";

        private bool WasNorthPressed
        {
            get { return Input.GetButtonDown(northButtonName); }
        }

        private bool WasEastPressed
        {
            get { return Input.GetButtonDown(eastButtonName); }
        }

        private bool WasWestPressed
        {
            get { return Input.GetButtonDown(westButtonName); }
        }

        private bool WasSouthPressed
        {
            get { return Input.GetButtonDown(southButtonName); }
        }

        public void Update()
        {
            if (WasNorthPressed)
                SelectDirection(Vector2.up * 100);
            else if (WasEastPressed)
                SelectDirection(Vector2.right * 100);
            else if (WasWestPressed)
                SelectDirection(Vector2.left * 100);
            else if (WasSouthPressed)
                SelectDirection(Vector2.down * 100);
        }
    }
}
