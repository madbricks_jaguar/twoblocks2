﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInput
{
    public enum CommandType
    {
        North,
        East,
        West,
        South
    }
}
