﻿using System;

namespace GameInput
{
    public interface ITouchInput
    {
        event Action TouchStarted;
        event Action TouchEnded;

        bool IsEnable { get; }
    }
}
