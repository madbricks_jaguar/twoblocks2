﻿using System;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public class DPadInput : BaseInput, IDPadInput
    {
        public event Action<bool /*isEnable*/> EnabledStateChanged;

        public void DirectionPressed(Vector2 direction)
        {
            SelectDirection(direction);
        }

        public override void Switch()
        {
            base.Switch();
            if (EnabledStateChanged != null)
                EnabledStateChanged(IsEnable);
        }
    }
}
