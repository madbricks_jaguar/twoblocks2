﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInput
{
	public interface IInputManager 
	{
        event Action<CommandType> CommandUsed;

		event Action<Vector3, Vector3> TwoFingersTouch;

        bool WasCameraPressed { get; }

		Quaternion DeviceRotation { get; }

        void SwitchInput();
    }
}
