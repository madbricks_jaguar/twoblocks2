﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameInput.Detail{
	public class GyroscopeInput {
		
		private bool SupportsGyro 
		{
			get { return SystemInfo.supportsGyroscope; }
		}

		public void InitializeGyroscope()
		{
			if (SupportsGyro) {
				Input.gyro.enabled = true;
				Input.gyro.updateInterval = 0.5f;
			}
		}

		public Quaternion DeviceRotation
		{
			get{ return SupportsGyro ? new Quaternion(0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude * new Quaternion(0, 0, 1, 0) : Quaternion.identity; }
		}

		public GyroscopeInput()
		{
			
		}
	}
}