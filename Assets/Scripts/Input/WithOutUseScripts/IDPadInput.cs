﻿using System;
using UnityEngine;

namespace GameInput
{
    public interface IDPadInput
    {
        event Action<bool /*isEnable*/> EnabledStateChanged;

        void DirectionPressed(Vector2 direction);
    }
}
