﻿using System;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public class BaseInput
    {
        public event Action<CommandType> CommandUsed;

        [SerializeField]
        private float dotThreshold = 70;

        private Plane northSouth, eastWest;
        private bool isReady = false;

        public bool IsEnable
        {
            get;
            private set;
        }

        public virtual void Initialize()
        {
            isReady = true;
            northSouth = new Plane(Vector3.forward, Vector3.zero);
            eastWest = new Plane(Vector3.right, Vector3.zero);
        }

        protected void SelectDirection(Vector2 drag)
        {
            if (!isReady)
                Debug.LogError("The INITILIAZE of some input is missing");

            Vector3 spatialDrag = GetCameraRelativeDrag(drag);
            float eastWestDot = Vector3.Dot(eastWest.normal, spatialDrag);
            float northSouthDot = Vector3.Dot(northSouth.normal, spatialDrag);

            if (Mathf.Abs(eastWestDot) < Mathf.Abs(northSouthDot))
            {
                //Debug.Log("Threshold "+ Mathf.Abs(eastWestDot));
                if (Mathf.Abs(eastWestDot) < dotThreshold)
                    CheckEastWest(northSouthDot);
            }
            else
            {
                //Debug.Log("Threshold " + Mathf.Abs(northSouthDot));
                if (Mathf.Abs(northSouthDot) < dotThreshold)
                    CheckNorthSouth(eastWestDot);
            }
        }

        private void CheckNorthSouth(float eastWestDot)
        {
            //Debug.Log("Check North South");
            if (Mathf.Sign(eastWestDot) > 0)
                RaiseCommandUsed(CommandType.South);
            else
                RaiseCommandUsed(CommandType.North);
        }

        private void CheckEastWest(float northSouthDot)
        {
            //Debug.Log("Check East West");
            if (Mathf.Sign(northSouthDot) > 0)
                RaiseCommandUsed(CommandType.East);
            else
                RaiseCommandUsed(CommandType.West);
        }

		protected Vector3 GetCameraRelativeDrag(Vector2 drag)
        {
            return Camera.main.transform.rotation * drag;
        }

        private void RaiseCommandUsed(CommandType command)
        {
            if (CommandUsed != null)
                CommandUsed(command);
        }

        public virtual void Switch()
        {
            IsEnable = !IsEnable;
        }
    }
}
