﻿using System;
using UnityEngine;

namespace GameInput.Detail
{
    [Serializable]
    public class TouchInput : BaseInput, ITouchInput
    {
        public event Action TouchStarted;
        public event Action TouchEnded;
		public event Action<Vector3, Vector3> TwofingersTouch;

        [SerializeField]
        private TouchInputManager touchInput;
        [SerializeField]
		protected float maxSwipeTime = 1f;
        [SerializeField, Range(1, 100)]
        private float minSwipeMagnitude = 10f;

		protected ITouchInfo PrimaryTouch
        {
            get { return touchInput.InputDevice.PrimaryTouch; }
        }

		protected ITouchInfo SecondaryTouch
		{
			get { return touchInput.InputDevice.SecondaryTouch; }
		}

		protected float MinDisplacement
        {
            get { return (float)Screen.height * (minSwipeMagnitude / 100); }
        }

		public virtual void Update()
        {
            if (IsEnable)
            {
                if (PrimaryTouch.StartedTouchThisFrame)
                    RaiseTouchStarted();

                if (PrimaryTouch.ReleasedTouchThisFrame)
                    CheckSwipe();
            }

			if (PrimaryTouch.IsTouching && SecondaryTouch.IsTouching)
				RaiseTwoFingersTouch ();
        }

		protected virtual void CheckSwipe()
        {
            RaiseTouchEnded();
            bool displacement = PrimaryTouch.DragVector.magnitude > MinDisplacement;
            bool time = Time.time - PrimaryTouch.StartTouchTime <= maxSwipeTime;
            if (displacement && time)
                SelectDirection(PrimaryTouch.DragVector);
        }

		protected void RaiseTouchStarted()
        {
            if (TouchStarted != null)
                TouchStarted();
        }

		protected void RaiseTouchEnded()
        {
            if (TouchEnded != null)
                TouchEnded();
        }

		protected void RaiseTwoFingersTouch()
		{
			if (TwofingersTouch != null)
				TwofingersTouch (PrimaryTouch.TouchPosition, SecondaryTouch.TouchPosition);
		}
    }
}
