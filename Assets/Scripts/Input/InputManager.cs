﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public event System.Action OnTapped;
    public event System.Action<DraggedDirection> OnDragged;
    public event System.Action<float, DraggedDirection> OnDraggedMagnitud;


    public static InputManager Instance { get; private set; }

    public float MinSwipeMagnitude = 100;

    private Vector3 startPos = Vector3.zero;



    private void Awake()
    {
        Instance = this;
    }

    private void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            OnTapped?.Invoke();
            startPos = Input.mousePosition;
        }
        if (Input.GetMouseButtonUp(0))
        {
            OnDragged?.Invoke(GetDragDirection(Input.mousePosition - startPos));
            OnDraggedMagnitud?.Invoke(GetDraggingForce(Input.mousePosition - startPos),GetDragDirection(Input.mousePosition - startPos));

        }

#else
        if (Input.touchCount == 0) return;
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            startPos = Input.GetTouch(0).position;
            OnTapped?.Invoke();
        }
        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            OnDragged?.Invoke(GetDragDirection((Vector3)Input.GetTouch(0).position - startPos));
            OnDraggedMagnitud?.Invoke(GetDraggingForce((Vector3)Input.GetTouch(0).position - startPos), GetDragDirection((Vector3)Input.GetTouch(0).position - startPos));
        }
        
#endif
    }

    public DraggedDirection GetDragDirection(Vector3 dragVector)
    {
        float positiveX = Mathf.Abs(dragVector.x);
        float positiveY = Mathf.Abs(dragVector.y);
        

        if (positiveX + positiveY < MinSwipeMagnitude)
        {
            return DraggedDirection.None;
           
        }
        DraggedDirection draggedDir = DraggedDirection.None;
        if (positiveX > positiveY)
        {
            draggedDir = (dragVector.x > 0) ? DraggedDirection.Right : DraggedDirection.Left;
          
        }
        else
        {
            draggedDir = (dragVector.y > 0) ? DraggedDirection.Up : DraggedDirection.Down;
        }
//        Debug.Log(draggedDir);
        return draggedDir;
    }


    public float GetDraggingForce(Vector3 dragVector)
    {
        return dragVector.magnitude;
    }
}

public enum DraggedDirection
{
    None,
    Up,
    Down,
    Right,
    Left
}
