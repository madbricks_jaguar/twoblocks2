﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial_Logic : MonoBehaviour
{

    void Start()
    {
        InputManager.Instance.OnDragged += CheckDirection;
    }

    void CheckDirection(DraggedDirection direct)
    {
        if (direct != DraggedDirection.None)
        {
            gameObject.SetActive(false);

        }
    }

    private void OnDisable()
    {
        InputManager.Instance.OnDragged -= CheckDirection;

    }
}
