﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysController : MonoBehaviour
{

    public Material materialWeak;
    [HideInInspector]
    public AIBehaviour[] enemys;
    [HideInInspector]
    public PowerUp[] powerUps;
    public float timeWeakness = 6;

    void Awake()
    {
        GameObject[] _power = GameObject.FindGameObjectsWithTag("PowerUp");
        powerUps = new PowerUp[_power.Length];

        for (int i = 0; i < powerUps.Length; i++)
        {
            powerUps[i] = _power[i].GetComponent<PowerUp>();
            powerUps[i].OnCatched += ChangeEnemyWeak;
        }

        GameObject[] _enemys = GameObject.FindGameObjectsWithTag("Enemy");
        enemys = new AIBehaviour[_enemys.Length]; //enemys


        for (int i =0; i<enemys.Length;i++)
        {
            enemys[i] = _enemys[i].GetComponent<AIBehaviour>();
            enemys[i].OnStepChosen += CheckQuadsEquals;

        }
    }

    void ChangeEnemyWeak()
    {
        foreach (AIBehaviour ia in enemys)
        {
            ia.weak = true;
            // ia.GetComponent<Renderer>().material = materialWeak;
            ParticleSystem[] particles = ia.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem particle in particles)
            {
                particle.Stop();
            }
        }
       StartCoroutine(WeaknessEnd());
       StartCoroutine(WeaknessChangeAdvice());
    }

    IEnumerator WeaknessEnd()
    {
        yield return new WaitForSeconds(timeWeakness);

        foreach (AIBehaviour ia in enemys)
        {
            ia.weak = false;
            //ia.RestoreMaterial();
            ParticleSystem[] particles = ia.GetComponentsInChildren<ParticleSystem>();
            foreach (ParticleSystem particle in particles)
            {
                particle.Play();
            }
        }

        foreach (PowerUp power in powerUps)
        {
            power.RestoreColliders();
        }
    }

    IEnumerator WeaknessChangeAdvice()
    {
        yield return new WaitForSeconds(timeWeakness-0.9f);

        foreach (AIBehaviour ia in enemys)
        {
            if (ia.gameObject.activeInHierarchy)
            {
                ia.StartCoroutine(ia.Twinkle(materialWeak));
            }
        }
    }

    private bool CheckQuadsEquals(AIBehaviour partner)
    {
        for (int i = 0; i < enemys.Length; i++)
        {
            for (int j = 0; j < enemys.Length; j++)
            {

                if (i != j)
                {
                    if (enemys[i].movement.nextQuad != null && enemys[j].movement.nextQuad!= null)
                    {
                        if (ReferenceEquals(enemys[i].movement.nextQuad, enemys[j].movement.nextQuad))
                        {
                           // print(enemys[i] + " y " + enemys[j]);

                            return true;
                        }
                    }
                }
            }
        }
        return false;
        
    }
}
