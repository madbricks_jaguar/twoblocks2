﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MyBox;

public class AIBehaviour : MonoBehaviour
{

    [HideInInspector]
    public  PlayerController movement;
    [SerializeField] private GridManager gridManager;
    public TriggerDetector triggerDetector;
    
    private BehaviourEnemy enemyBehaviour = BehaviourEnemy.Patrol;

    [ConditionalField(nameof(enemyBehaviour), false, BehaviourEnemy.Follow)] private TargetEnemy targetFollow;

    [Separator("Path")]
    [TooltipAttribute("the Behaviour must be ~Patrol~")]
    public Transform[] pathPoints;


    [HideInInspector]
    public bool weak;
    [HideInInspector]
    public int indexPath =0;
    
    public event System.Func<AIBehaviour, bool> OnStepChosen;

    private Material bufferMaterial;
    private Renderer render;

    private void Start()
    {
        movement = GetComponent<PlayerController>();
        gridManager = GameObject.FindWithTag("GameController").GetComponent<GridManager>();

       

        triggerDetector.OnTriggerEntered += CollisionWithBox;

        render = GetComponent<Renderer>();
        bufferMaterial = render.material;
        
    }
    private void OnEnable()
    {
        Invoke("StartPosition", 0.4f);

    }

    private void OnDisable()
    {
        StopAllCoroutines();
        movement.nextQuad.DisableIndicator();
    }

    private void StartPosition()
    {
        ColorChanging target = gridManager.FindPositions(transform.position);
        movement.SetNextQuad(target);
    }


    public NodeContainer GetTarget()
    {
        return FollowPath();
    }

    public NodeContainer FollowPath()
    {
        if ((int)transform.position.x == (int)pathPoints[indexPath].position.x && (int)transform.position.z == (int)pathPoints[indexPath].position.z)
        {
            indexPath++; 

            if (indexPath == pathPoints.Length)
            {
                indexPath = 0;
            }
        }
        if (gridManager == null)
        {
            gridManager = GameObject.FindWithTag("GameController").GetComponent<GridManager>();
        }

        NodeContainer targetPos = gridManager.FindPositions(pathPoints[indexPath].position,true);
//        Debug.Log("index path " + indexPath+" is "+targetPos.name);

        return targetPos;
    }

    public IEnumerator MoveEnemy(Vector3 stepDirect)
    {
        if (stepDirect + Vector3.up == transform.position)
        {
            yield break;
           // return;
        }

        NodeContainer target = gridManager.FindPositions(stepDirect,true);

        if (target != null)
        {
            movement.SetNextQuad(target.GetComponent<ColorChanging>());
            movement.nextQuad.ActiveIndicator();
        }

        if (OnStepChosen !=null && OnStepChosen.Invoke(this))
        {
            movement.nextQuad.DisableIndicator();
            movement.SetPreviousQuad();
            BackPreviousPathPoint();
            yield break;
        }
        yield return new WaitForSeconds(0.6f);

        if (target != null)
        {
            movement.Move(target.node.position + Vector3.up);
            movement.nextQuad.Invoke("DisableIndicator",movement.timeAnimation);
        }
    }

    private void CollisionWithBox(Collider other)
    {
        if (weak)
        {
            movement.ActiveExplotion();
            movement.SetNextQuad(null);
            gameObject.SetActive(false);
        }
        else
        {
            other.GetComponent<PlayerController>().ActiveExplotion();
        }

        if (other.CompareTag("Enemy"))
        {
            movement.ActiveExplotion();
        }
    }

    private void BackPreviousPathPoint()
    {
            indexPath--;

            if (indexPath < 0)
            {
                indexPath = 0;
            }
    }

    public IEnumerator Twinkle(Material mat)
    {
        bool _switch = false;

        while (weak)
        {
             _switch = !_switch;
            render.material =(_switch)? bufferMaterial: mat;
            yield return new WaitForSeconds(0.18f);
        }
    }

    public void RestoreMaterial()
    {
        StopCoroutine(Twinkle(bufferMaterial));
        render.material = bufferMaterial;   
    }

    public enum BehaviourEnemy
    {
       Follow,
       Patrol
       //Static
    }

    public enum TargetEnemy
    {
        bluBox,
        pinkBox
    }
}