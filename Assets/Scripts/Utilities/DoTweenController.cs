﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using UnityEngine.Events;
using TMPro;

public class DoTweenController : MonoBehaviour
{
    public float _duration;
    public float delay;
    public bool playOnAwake;

    public Vector3 targetScale;
    public Vector3 targetPosition;
    public Vector3 rotation;
    public Vector3 originalScale { get; private set; }
    public  Vector3 originalPosition { get;  set; }
    public Vector3 originalLocalPosition { get; private set; }
    private Color colorObject;
    private TextMeshProUGUI textImage;


    public enum AnimationType
    {
        scale,
        position,
        localPosition,
        localReturnPosition,
        rotate,
        UIScale,
        UIPositionFadeOut,
        UIPositionFadeIn,
        UIPositionLoop,
        UIFadeInText,
        jump
    }
    public AnimationType _animationType;

    public Ease moveEase = Ease.Linear;

    public UnityEvent eventAfterAnimation;


    private void Awake()
    {
        originalScale = transform.localScale;
        originalPosition = transform.position;
        originalLocalPosition = transform.localPosition;

        if (playOnAwake)
        {
            Invoke("CheckAnimation",delay);
        }
    }

    private void OnEnable()
    {
        if (GetComponent<TextMeshProUGUI>())
        {
            textImage = GetComponent<TextMeshProUGUI>();

            colorObject = textImage.color;
            colorObject.a = 1;
            textImage.color = colorObject;
        }

        if (playOnAwake)
        {
            transform.localScale = originalScale;
            transform.position = originalPosition;
            Invoke("CheckAnimation", delay);
        }
    }

    public void CheckAnimation()
    {
        StartCoroutine(StartEvent());

        switch (_animationType)
        {
            case AnimationType.position:

                transform.position = originalPosition;
                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                transform.DOLocalMove(transform.position+targetPosition, _duration,false).SetEase(moveEase);
                break;

            case AnimationType.localPosition:

                transform.localPosition = originalLocalPosition;
                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                transform.DOLocalMove(targetPosition, _duration, false).SetEase(moveEase);
                break;

            case AnimationType.localReturnPosition:

                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                transform.DOLocalMove(originalLocalPosition, _duration, false).SetEase(moveEase);
                break;


            case AnimationType.scale:

                transform.localScale = originalScale;
                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                break;

            case AnimationType.rotate:
                transform.DORotate(rotation, _duration, RotateMode.Fast).SetEase(moveEase).SetLoops(2, LoopType.Yoyo);

                break;

            case AnimationType.UIScale:

                transform.DOScale(targetScale, _duration).SetEase(moveEase);
                break;

            case AnimationType.jump:

                transform.DOJump(targetPosition,3,1,_duration,false).SetEase(moveEase);
                break;

            case AnimationType.UIPositionFadeOut:

                var _seq = DOTween.Sequence();

                GetComponent<RectTransform>().DOAnchorPos(targetPosition,_duration,false).SetEase(moveEase).SetLoops(5,LoopType.Yoyo);
                _seq.Insert((delay+_duration)*1.2f,GetComponent<Image>().DOFade(0,0.5f));
                break;

            case AnimationType.UIPositionFadeIn:

                var _sequ = DOTween.Sequence();

                GetComponent<RectTransform>().DOAnchorPos(targetPosition, _duration, false).SetEase(moveEase).SetLoops(5, LoopType.Yoyo);
                _sequ.Insert(delay, GetComponent<Image>().DOFade(1, 0.5f));
                break;

            case AnimationType.UIPositionLoop:


                GetComponent<RectTransform>().DOAnchorPos(targetPosition, _duration, false).SetEase(moveEase).SetLoops(60, LoopType.Yoyo);
                break;

            case AnimationType.UIFadeInText:

                textImage.DOFade(0, 0.5f).SetEase(moveEase);
                break;
        }
    }

    IEnumerator StartEvent()
    {
        yield return new WaitForSeconds((delay + _duration) * 1.5f);

        if (eventAfterAnimation != null)
        {
            eventAfterAnimation.Invoke();

        }

    }
}