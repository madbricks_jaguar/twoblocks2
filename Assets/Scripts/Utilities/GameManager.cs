﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameManager : MonoBehaviour
{
    int level = 0;
    public static GameManager instance = null;
    List<int> subLevels = new List<int>();
    [SerializeField] GameObject[] crowns;
    int subLevelsIndex = 0;
    [HideInInspector]
    public int currentLevel;

    [SerializeField] GameObject congratulationImage;
    [SerializeField] GameObject particlesWin1;
    [SerializeField] GameObject particlesWin2;
    [SerializeField] GameObject particlesWinAllLevels;
    [SerializeField] TextMeshProUGUI textLevelCongratulations;
    [SerializeField] TextMeshProUGUI textCurrentLevel;
    [SerializeField] TextMeshProUGUI textNextLevel;
    [SerializeField] TextMeshProUGUI textLose;
    [SerializeField] TextMeshProUGUI textSteps;
    GameObject buttonSubLevel;
    [SerializeField] GameObject buttonLevel;


    private void Awake()
    {
        
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Destroy(gameObject);
            textSteps.text = "";
        }

        if (instance == this)
        {
            DontDestroyOnLoad(gameObject);
            transform.GetChild(0).gameObject.SetActive(true);
            LoadData();
            textCurrentLevel.text = "" + level;

            #if UNITY_EDITOR
                PlayerPrefs.DeleteAll();
                PlayerPrefs.SetInt(KeysStorage.HEARTS,3);
            #else

            #endif

        }

    }

    public void NextScenary()
    {
        subLevelsIndex++;
        if (subLevelsIndex < crowns.Length)
        {
            buttonSubLevel.SetActive(true);
            crowns[subLevelsIndex - 1].SetActive(true);
            particlesWin1.SetActive(true);
            particlesWin1.transform.position = Camera.main.transform.forward * 4 + Camera.main.transform.up * -6 + Camera.main.transform.right * 4;
            particlesWin2.SetActive(true);
            particlesWin2.transform.position = Camera.main.transform.forward * 4 + Camera.main.transform.up * -6 + Camera.main.transform.right * -2;
        }
        else
        {
            SplitTextCongratulations(textLevelCongratulations.text);
            crowns[subLevelsIndex - 1].SetActive(true);
            congratulationImage.SetActive(true);
            buttonLevel.SetActive(true);
            particlesWinAllLevels.SetActive(true);
            particlesWinAllLevels.transform.position = Camera.main.transform.position;
            particlesWin1.SetActive(true);
            particlesWin1.transform.position = Camera.main.transform.forward * 4 + Camera.main.transform.up * -6 + Camera.main.transform.right * 4;
            particlesWin2.SetActive(true);
            particlesWin2.transform.position = Camera.main.transform.forward * 4 + Camera.main.transform.up * -6 + Camera.main.transform.right * -2;
        }

        textSteps.text = "";
    }

    public void NextLevel()
    {
        FillLevels();
        level++;
        subLevelsIndex = 0;
        LoadNextLevel();
        SaveData();
        congratulationImage.SetActive(false);
        textCurrentLevel.text = "" + level;
        textNextLevel.text = "" + (level + 1);
        foreach (GameObject _crown in crowns)
        {
            _crown.SetActive(false);
        }
        textSteps.text = "";

    }

    public void LoadNextLevel()
    {
        textSteps.text = "";
        particlesWin1.SetActive(false);
        particlesWin2.SetActive(false);
        particlesWinAllLevels.SetActive(false);
        SceneManager.LoadSceneAsync(subLevels[subLevelsIndex]);
    }

    void FillLevels()
    {
        subLevels.Clear();

        if (SceneManager.GetActiveScene().buildIndex != 0)
        {
            subLevels.Add(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {
            subLevels.Add(Random.Range(1, SceneManager.sceneCountInBuildSettings));

        }

        if (!CheckSublevelsAmount())
        {
            subLevels.Add(Random.Range(1, SceneManager.sceneCountInBuildSettings));
            subLevels.Add(Random.Range(1, SceneManager.sceneCountInBuildSettings));
            subLevels.Add(Random.Range(1, SceneManager.sceneCountInBuildSettings));
            return;
        }

        while (subLevels.Count < 4)
        {
            int randomNumber = Random.Range(1, SceneManager.sceneCountInBuildSettings);
            if (!subLevels.Contains(randomNumber))
            {
                subLevels.Add(randomNumber);
            }
        }
    }

    public IEnumerator BackFirstSublevel()
    {
        particlesWin1.SetActive(false);
        particlesWin2.SetActive(false);
        particlesWinAllLevels.SetActive(false);

        textLose.gameObject.SetActive(true);

        yield return new WaitForSeconds(1f);

        BackMainScene();
        textLose.gameObject.SetActive(false);
    }

    void SplitTextCongratulations(string congratulationText)
    {
        string[] resultString = congratulationText.Split('@');

        textLevelCongratulations.text = "Level "+level+" Complete";

        textLevelCongratulations.text = resultString[0] + "" + level + "" + resultString[1];

    }

    void SaveData()
    {
         PlayerPrefs.SetInt(KeysStorage.LEVEL, level);
    }

    void LoadData()
    {
         level = PlayerPrefs.GetInt(KeysStorage.LEVEL, 0);
    }

    bool CheckSublevelsAmount()
    {
        if (SceneManager.sceneCountInBuildSettings < 5)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    public void SetStepsText(string info,bool showInfo)
    {
        if (showInfo)
        {
            textSteps.text = info;
        }
        else
        {
            textSteps.text = "";
        }
    }

    public void BackMainScene()
    {
        textSteps.text = "";
        PlayerPrefs.SetInt(KeysStorage.HEARTS,3);
        SceneManager.LoadScene(0);
    }

    public void CheckCurrentLevel()
    {
        int newLvel = PlayerPrefs.GetInt(KeysStorage.LEVEL, 0);


        if (currentLevel == newLvel)
        {
           // print(currentLevel + " H " + newLvel);
            newLvel++;
            PlayerPrefs.SetInt(KeysStorage.LEVEL, newLvel);
        }

        textSteps.text= "";
        buttonLevel.SetActive(true);

    }

    private void OnDisable()
    {

        #if UNITY_EDITOR
        #endif


    }

}
