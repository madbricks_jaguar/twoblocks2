﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class TriggerDetector : MonoBehaviour
{
    public event System.Action<Collider>OnTriggerEntered;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Box") || other.CompareTag("Box2"))
        {
            OnTriggerEntered?.Invoke(other);
        }
    }
}
