﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableByTime : MonoBehaviour
{

    [SerializeField] float timeToDisable;

    private void OnEnable()
    {
        CancelInvoke("DisableAfter");
        Invoke("DisableAfter",timeToDisable);

    }

    void DisableAfter()
    {
        gameObject.SetActive(false);

    }
}
