﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{

    [SerializeField] private PlayerController box1;
    [SerializeField] private PlayerController box2;
    public List<LevelOnMainScene> levelObjects = new List<LevelOnMainScene>();
    [SerializeField] private LevelsList sceneList;
    [SerializeField] private CameraSwipeMove cameraSwipeable;
     public int splitLevels =3;
    private int splitBuffer;
    
    private string currentLevelName;
    public int currentLevelIndex { get; set;}


    void Start()
    {
        splitBuffer = splitLevels;
        splitLevels += PlayerPrefs.GetInt(KeysStorage.LAST_SUBLEVEL,0);

        FillLevelObjects();
        cameraSwipeable.lowerLimit = levelObjects[0].transform.position.z -10;
        cameraSwipeable.upperLimit = levelObjects[splitLevels].transform.position.z-15;

        //if (PlayerPrefs.GetInt(KeysStorage.LEVEL) < splitLevels)
        //{
        //    LoadNextScenary();
        //}
        //else if (PlayerPrefs.GetInt(KeysStorage.LEVEL) > splitLevels)
        //{
        //    LoadNextScenary();
        //}
    }

    private void MoveToLevel(string _levelName, LevelOnMainScene _levelObj)
    {
        box1.Move(_levelObj.box1Target.transform.position + Vector3.up);
        box2.Move(_levelObj.box2Target.transform.position + Vector3.up);

        currentLevelName = _levelObj.levelName;
        currentLevelIndex = _levelObj.levelIndex;

        for (int i = 0; i < levelObjects.Count; i++)
        {
            levelObjects[i].OnTapped -= MoveToLevel;
        }
        Invoke("LoadNextLevel",box1.timeAnimation+0.4f);
    }


    private void FillLevelObjects()
    {
        if (levelObjects.Count != sceneList.levels.Count)
        {
            int diferenceScenesObjects = levelObjects.Count - sceneList.levels.Count;

            if (diferenceScenesObjects < 0)
            {
                for (int i = levelObjects.Count; i < sceneList.levels.Count; i++)
                {
                    Vector3 postionNewLevelObject = levelObjects[levelObjects.Count - 1].transform.position + Vector3.forward * 6;
                    LevelOnMainScene newLevelObject = Instantiate(levelObjects[0], postionNewLevelObject, Quaternion.identity, transform);
                    levelObjects.Add(newLevelObject);
                }
            }
            else if (diferenceScenesObjects > 0)
            {
                for (int i = levelObjects.Count; i < sceneList.levels.Count; i++)
                {
                    Vector3 postionNewLevelObject = levelObjects[levelObjects.Count - 1].transform.position + Vector3.forward * 6;
                    LevelOnMainScene newLevelObject = Instantiate(levelObjects[0], postionNewLevelObject, Quaternion.identity, transform);
                    levelObjects.Add(newLevelObject);
                }
            }
        }

        int currentLevel = 0;

        for (int i = 0; i < sceneList.levels.Count; i++)
        {
            levelObjects[i].OnTapped += MoveToLevel;
            levelObjects[i].levelName = sceneList.levels[i].nameScene;
            levelObjects[i].levelIndex = sceneList.levels[i].numberLevel;

            if (levelObjects[i].levelIndex > PlayerPrefs.GetInt(KeysStorage.LEVEL))
            {
                levelObjects[i].imageLevelPassed.SetActive(false);
            }

            if (PlayerPrefs.GetString(KeysStorage.LAST_LEVEL_LOADED).Equals(levelObjects[i].levelName))
            {
                currentLevel = i;
                currentLevelIndex = currentLevel;
            }

            if (i >= splitLevels)
            {
                levelObjects[i].gameObject.SetActive(false);
            }
        }

        box1.transform.position = levelObjects[currentLevel].box1Target.position + Vector3.up;
        box2.transform.position = levelObjects[currentLevel].box2Target.position + Vector3.up;

        SetCameraPosition(currentLevel);
    }

    private void SetCameraPosition(int indexLevelObject)
    {
        Vector3 cameraSetPosit = new Vector3(cameraSwipeable.transform.position.x, cameraSwipeable.transform.position.y, levelObjects[indexLevelObject].transform.position.z - 5);

        if (PlayerPrefs.GetInt(KeysStorage.LEVEL) < splitLevels)
        {
            cameraSwipeable.transform.position = cameraSetPosit - Vector3.forward * 5f;
            cameraSwipeable.targetPosition = cameraSetPosit;
        }
        else
        {
            cameraSwipeable.transform.position = cameraSetPosit - Vector3.forward * 15f;
            cameraSwipeable.targetPosition = cameraSetPosit - Vector3.forward * 10f;
        }
    }

    private void LoadNextLevel()
    {
        GameManager.instance.currentLevel = currentLevelIndex;
        SceneManager.LoadScene(currentLevelName);
    }

    public void  LoadNextScenary()
    {
        if (PlayerPrefs.GetInt(KeysStorage.LEVEL) < splitLevels || sceneList.levels.Count < splitLevels + splitBuffer)
        {
            return;
        }

        for (int i = 0; i < splitLevels + splitBuffer; i++)
        {
            levelObjects[i].gameObject.SetActive(true);
        }

        cameraSwipeable.upperLimit = levelObjects[levelObjects.Count - 1].transform.position.z - 15;

        PlayerPrefs.SetInt(KeysStorage.LAST_SUBLEVEL, splitLevels);
    }
}
