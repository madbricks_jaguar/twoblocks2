﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelOnMainScene : MonoBehaviour
{
    public Transform box1Target;
    public Transform box2Target;
    public GameObject imageLevelPassed;
    [HideInInspector]
    public string levelName;
    [HideInInspector]
    public int levelIndex;
    public event System.Action<string,LevelOnMainScene> OnTapped;
    bool canPlayLevel;


    private void OnMouseOver()
    {
        canPlayLevel = true;
    }

    private void OnMouseExit()
    {
        canPlayLevel = false;
    }

    private void OnMouseUp()
    {
         if (!canPlayLevel) { return; }

        if (!imageLevelPassed.activeInHierarchy)
        {
            return;
        }

        OnTapped?.Invoke(levelName,this);
        PlayerPrefs.SetInt(KeysStorage.HEARTS,3);
        PlayerPrefs.SetString(KeysStorage.LAST_LEVEL_LOADED, levelName);
    }
}
