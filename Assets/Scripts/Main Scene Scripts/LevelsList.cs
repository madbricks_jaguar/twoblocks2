﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Levels List", menuName = "Inventory/Levels List")]
public class LevelsList : ScriptableObject
{
    public List<LevelFeatures> levels = new List<LevelFeatures>();
}

[System.Serializable]
public class LevelFeatures
{
    public string nameScene;
    public int numberLevel;

}