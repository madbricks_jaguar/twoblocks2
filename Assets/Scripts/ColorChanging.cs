﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorChanging : MonoBehaviour
{

    private ScoringSystem scoreSys;
    bool collected;
    public Material[] material;
    Renderer rend;
    public GameObject indicator;

    // [HideInInspector]
    // public int indexInGrid;
    [HideInInspector]
    public GameObject isSomethingOverme;
    
    void Awake()
    {
        scoreSys = GameObject.FindGameObjectWithTag("GameController").GetComponent<ScoringSystem>();
        rend = GetComponent<MeshRenderer>();
    }


    public void ChangeColor(string tagCollision) 
    {
        if (rend == null)
        {
            rend = GetComponent<MeshRenderer>();
        }

        //rend.enabled = true;
        if (tagCollision == "Box")
        {
            rend.material = material[0];
            scoreSys.GetComponent<GridManager>().SetBoxPosition(tagCollision, gameObject);
            AddToScoringSystem();
        }
        else if (tagCollision == "Box2")
        {
            rend.material = material[1];
            scoreSys.GetComponent<GridManager>().SetBoxPosition(tagCollision, gameObject);
            AddToScoringSystem();
        }
        else if(tagCollision == "Enemy")
        {
            rend.material  = material[2];
            RemoveFromScoringSystem();
        }
    }

    void AddToScoringSystem()
    {
        if (!collected)
        {
            if (scoreSys == null)
            {
                scoreSys = GameObject.FindGameObjectWithTag("GameController").GetComponent<ScoringSystem>();
            }
            scoreSys.theScore += 1;
            collected = true;
        }
    }

    void RemoveFromScoringSystem()
    {
        if (collected)
        {
            if (scoreSys == null)
            {
                scoreSys = GameObject.FindGameObjectWithTag("GameController").GetComponent<ScoringSystem>();
            }
            scoreSys.theScore -= 1;
            collected = false;
        }
    }


    public void ResetColor()
    {
        rend.sharedMaterial = material[2];
        collected = false;
    }

    public void ActiveIndicator()
    {
        indicator.SetActive(true);
    }

    public void DisableIndicator()
    {
        if (indicator != null)
        {
            indicator.SetActive(false);

        }
    }

}
