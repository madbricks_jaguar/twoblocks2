﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using MyBox;

[RequireComponent(typeof(GridManager))]
public class ScoringSystem : MonoBehaviour
{
    public int theScore;
  //  [HideInInspector]
    public int scoreGoal=0;
    protected int lifes = 3;

    public bool useStepsLimits;
    [ConditionalField("useStepsLimits")] public int limitSteps;
    int steps =0;
    private PlayerController box;
    private PlayerController box2;
    public event  System.Action OnRestartLevel;

    bool completed;


    private void Start()
    {
        box = GameObject.FindGameObjectWithTag("Box").GetComponent<PlayerController>();
        box2 = GameObject.FindGameObjectWithTag("Box2").GetComponent<PlayerController>();
        if (GameManager.instance == null)
        {
            Debug.LogError("DRAG A GAME MANAGER PREFAB TO SCENE");
        }

        GameManager.instance.SetStepsText("Steps:" + steps + "/" + limitSteps, useStepsLimits);

        lifes = PlayerPrefs.GetInt(KeysStorage.HEARTS);
    }

    void Update()
    {
        if (theScore == scoreGoal && !completed )
        {
            box.SetPathCompleted();
            box2.SetPathCompleted();
            NextLevel();
            completed = true;
        }

        if (theScore == 0)
        {
            gameObject.SendMessage("FindStartPositions");

        }
    }

   public bool SubstractLife() 
   {
        lifes--;

        if (lifes <= 0)
        {
            return true;
        }
        else
        {
            PlayerPrefs.SetInt(KeysStorage.HEARTS, lifes);
            return false;
        }
   }

   private void NextLevel()
   {
        box.SetCanMove(true);
        box2.SetCanMove (true);
        GameManager.instance.CheckCurrentLevel();
   }

    public void AddStep()
    {
        if (useStepsLimits)
        {
            steps++;
            GameManager.instance.SetStepsText("Steps:" + steps + "/" + limitSteps,useStepsLimits);

            if (steps > limitSteps)
            {
                GameManager.instance.StartCoroutine(GameManager.instance.BackFirstSublevel());
                box.ActiveExplotion();
                box2.ActiveExplotion();
                completed = true;
            }
        }
    }
    
}



