﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowObject : MonoBehaviour
{
    [SerializeField] PlayerController _player;
    [SerializeField] private Vector3 offsetPos;

    private void OnEnable()
    {
        transform.SetParent(null);
        _player.OnPlayerMove += SetPosition;
        Invoke("DisableHeart",0.25f);
    }

    private void OnDisable()
    {
        _player.OnPlayerMove -= SetPosition;
    }

    void SetPosition() 
   {
        transform.position = _player.transform.position + offsetPos;
   }

    void DisableHeart() 
    {
        for (int i = 0; i < (3); i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        int heartsCount = PlayerPrefs.GetInt(KeysStorage.HEARTS);

        for (int i = 0; i<(heartsCount); i++) 
        {
           transform.GetChild(i).gameObject.SetActive(true);
        }
    }
}
