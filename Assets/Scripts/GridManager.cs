﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ScoringSystem))]
public class GridManager : MonoBehaviour
{
    ScoringSystem SS;
    [HideInInspector]
    public PlayerController box1;

    [HideInInspector]
    public PlayerController box2;
   
    [HideInInspector]
    public GameObject floor;
    [HideInInspector]
    public NodeContainer[] quads;

    [HideInInspector]
    public int indexBox1;
    [HideInInspector]
    public int indexBox2;

    private int startIndexbox1;
    private int startIndexbox2;

    private void Start()
    {
        floor = GameObject.FindGameObjectWithTag("Floor");
        box1 = GameObject.FindGameObjectWithTag("Box").GetComponent<PlayerController>();
        box2 = GameObject.FindGameObjectWithTag("Box2").GetComponent<PlayerController>();

        SS = GetComponent<ScoringSystem>();
        quads = new NodeContainer[floor.transform.childCount];

        for (int i = 0; i < quads.Length; i++)
        {
            quads[i] = floor.transform.GetChild(i).GetComponent<NodeContainer>();
        }
        SS.scoreGoal = quads.Length;
        

        for (int i = 0; i < quads.Length; i++)
        {
            for (int j = 0; j < quads.Length; j++)
            {

                if ((int)quads[i].transform.position.x + 1 == (int)quads[j].transform.position.x && (int)quads[i].transform.position.z == (int)quads[j].transform.position.z)
                {
                    quads[i].node.rightNeighbor = quads[j].node;
                    quads[j].node.leftNeighbor = quads[i].node;

                }
                if ((int)quads[i].transform.position.x - 1 == (int)quads[j].transform.position.x && (int)quads[i].transform.position.z == (int)quads[j].transform.position.z)
                {
                    quads[i].node.leftNeighbor = quads[j].node;
                    quads[j].node.rightNeighbor = quads[i].node;
                }
                if ((int)quads[i].transform.position.z - 1 == (int)quads[j].transform.position.z && (int)quads[i].transform.position.x == (int)quads[j].transform.position.x)
                {
                    quads[i].node.backNeighbor = quads[j].node;
                    quads[j].node.frontNeighbor = quads[i].node;
                }
                if ((int)quads[i].transform.position.z + 1 == (int)quads[j].transform.position.z && (int)quads[i].transform.position.x == (int)quads[j].transform.position.x)
                {
                    quads[i].node.frontNeighbor = quads[j].node;
                    quads[j].node.backNeighbor = quads[i].node;
                }

                //------------------- checkOverlaping

                if (quads[i].transform.position == quads[j].transform.position && !ReferenceEquals(quads[i],quads[j]))
                {
                    Debug.LogError("THERE ARE BLOCKS OVERLAPPED ("+quads[i].name+" & "+ quads[j].name+")");
                }
            }
        }

        //TODO esto v
        GetComponent<GridNodes>().SetGridNodes(quads);

        FindStartPositions();
        InputManager.Instance.OnDragged += MovePlayers;
        
    }

    void MovePlayers(DraggedDirection direction)
    {
        switch (direction)
        {
            case DraggedDirection.Right://-------------------------Swipe Right

                if (quads[indexBox1].node.rightNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox1].node.position + Vector3.right, box1, quads[indexBox1].node.rightNeighbor);
                }
                else  { box1.SendMessage("Fall", quads[indexBox1].transform.position + Vector3.right);}

                if (quads[indexBox2].node.leftNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox2].node.position - Vector3.right, box2, quads[indexBox2].node.leftNeighbor);
                }
                else
                {
                    box2.SendMessage("Fall", quads[indexBox2].transform.position - Vector3.right);
                }

                SS.AddStep();
                break;
            case DraggedDirection.Left://-------------------------Swipe Left

                if (quads[indexBox1].node.leftNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox1].transform.position - Vector3.right, box1, quads[indexBox1].node.leftNeighbor);
                }
                else
                { box1.SendMessage("Fall", quads[indexBox1].transform.position - Vector3.right); }

                if (quads[indexBox2].node.rightNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox2].transform.position + Vector3.right, box2, quads[indexBox2].node.rightNeighbor);

                }
                else
                {
                    box2.SendMessage("Fall", quads[indexBox2].transform.position + Vector3.right);
                }
                SS.AddStep();
                break;

            case DraggedDirection.Up: //-------------------------Swipe Up

                if (quads[indexBox1].node.frontNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox1].transform.position + Vector3.forward, box1, quads[indexBox1].node.frontNeighbor);

                }
                else
                {
                    box1.SendMessage("Fall", quads[indexBox1].transform.position + Vector3.forward);
                }

                if (quads[indexBox2].node.backNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox2].transform.position - Vector3.forward, box2, quads[indexBox2].node.backNeighbor);
                }
                else
                {
                    box2.SendMessage("Fall", quads[indexBox2].transform.position - Vector3.forward);
                }
                SS.AddStep();
                break;

            case DraggedDirection.Down: //-------------------------Swipe dOwn

                if (quads[indexBox1].node.backNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox1].transform.position - Vector3.forward, box1, quads[indexBox1].node.backNeighbor);
                }
                else
                {
                    box1.SendMessage("Fall", quads[indexBox1].transform.position - Vector3.forward);
                }

                if (quads[indexBox2].node.frontNeighbor != null)
                {
                    CheckNodePosition(quads[indexBox2].transform.position + Vector3.forward, box2, quads[indexBox2].node.frontNeighbor);
                }
                else
                {
                    box2.SendMessage("Fall", quads[indexBox2].transform.position + Vector3.forward);
                }
                SS.AddStep();
                break;
            default:
                return;
        }

    }

    void FindStartPositions()
    {
        bool box1Over = false;
        bool box2Over = false;

        for (int i = 0; i < quads.Length; i++)
        {
            if ((int)box1.transform.position.x == (int)quads[i].transform.position.x && (int)box1.transform.position.z == (int)quads[i].transform.position.z && !box1Over)
            {
                box1Over = true;
                indexBox1 = i;
                startIndexbox1 = i;
            }

            if ((int)box2.transform.position.x == (int)quads[i].transform.position.x && (int)box2.transform.position.z == (int)quads[i].transform.position.z && !box2Over)
            {
                box2Over = true;
                indexBox2 = i;
                startIndexbox2 = i;
            }

            if (box1Over && box2Over)
            {
                quads[indexBox1].GetComponent<ColorChanging>().ChangeColor(box1.tag);
                quads[indexBox2].GetComponent<ColorChanging>().ChangeColor(box2.tag);
                break;
            }

        }

        if (!box1Over && !box2Over)
        {
            quads[startIndexbox1].GetComponent<ColorChanging>().ChangeColor(box1.tag);
            quads[startIndexbox2].GetComponent<ColorChanging>().ChangeColor(box2.tag);
        }

    }

    public ColorChanging FindPositions(Vector3 pos)
    {
        ColorChanging gridPos;

        for (int i = 0; i < quads.Length; i++)
        {
            if ((int)pos.x == (int)quads[i].node.position.x && (int)pos.z == (int)quads[i].node.position.z)
            {
                gridPos = quads[i].GetComponent<ColorChanging>();
                return gridPos;
            }
        }
        return null;
    }

    public NodeContainer FindPositions(Vector3 pos, bool f)
    {
        NodeContainer gridPos;
       // print(quads.Length+" en "+pos+" e el nombre"+name);

        for (int i = 0; i < quads.Length; i++)
        {
            if ((int)pos.x == (int)quads[i].node.position.x && (int)pos.z == (int)quads[i].node.position.z)
            {
                gridPos = quads[i];
                return gridPos;
            }
        }
        return null;
    }

    public void SetBoxPosition(string tagBox,GameObject quadUnder)
    {
        if (tagBox.Equals("Box"))
        {
            for (int i = 0; i < quads.Length; i++)
            {
                if (ReferenceEquals(quads[i].gameObject,quadUnder))
                {
                    indexBox1 =i;
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < quads.Length; i++)
            {
                if (ReferenceEquals(quads[i].gameObject, quadUnder))
                {
                    indexBox2 = i;
                    break;
                }
            }
        }

    }

    public Vector3 GetBoxPosition(string tagBox)
    {
        if (tagBox.Equals("Box"))
        {
            return box1.transform.position;
        }
        else if (tagBox.Equals("Box2"))
        {
            return box2.transform.position;
        }
        else
        {
            return Vector3.one * 40000;
        }
    }

    public NodeContainer GetBoxPositionInQuads(string tagBox)
    {
        if (tagBox.Equals("Box"))
        {
            return quads[indexBox1];
        }
        else if (tagBox.Equals("Box2"))
        {
            return quads[indexBox2];
        }
        else
        {
            return null;
        }
    }

    public void CheckNodePosition(Vector3 posDestiny, PlayerController player, Node nodeTarget)
    {

        if ((int)posDestiny.x == (int)nodeTarget.position.x && (int)posDestiny.z == (int)nodeTarget.position.z)
        {
            player.Move(new Vector3(nodeTarget.position.x,nodeTarget.position.y,nodeTarget.position.z) + Vector3.up);
            player.SetNextQuad(nodeTarget.owner.GetComponent<ColorChanging>());

            if (nodeTarget.owner.GetComponent<PlataformMovement>())
            {
                player.transform.SetParent(nodeTarget.owner.transform);
            }
            else
            {
                player.transform.SetParent(null);
            }
        }
        else
        {
            player.Fall(posDestiny);
        }
    }


    public bool OverTherIsSomething(Vector3 position)
    {
        foreach (NodeContainer quad in quads)
        {
            if (position == quad.transform.position + Vector3.up)
            {
                return true;

            }
        }
        return false;
    }
}



