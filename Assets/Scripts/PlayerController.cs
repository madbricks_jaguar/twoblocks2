﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
using UnityEngine.Events;
    

public class PlayerController : MonoBehaviour
{
    [HideInInspector]
    public ColorChanging nextQuad;

    [HideInInspector]
    public ColorChanging previousQuad;


    public ScoringSystem scoringSys;

    private Animator _animator;
    [SerializeField] GameObject explotion;

    public delegate void _MoveEvent();
    public event _MoveEvent OnPlayerMove;

    //------------

    private Vector3 startPos;
    [HideInInspector]
    public Vector3 currentPos;
    [Range(0,5)]
    public float jumpForce = 1;
    [Range(0,5)]
    public float timeAnimation = 0.3f;

    public Ease moveStepCurveBegin = Ease.Linear;
    public Ease moveStepCurveEnd = Ease.Linear;

    [HideInInspector]
    public bool falling = false;

    public bool IsArrived { get; private  set; } = false;
    [HideInInspector]
    public bool canMove = true;
    public bool canLose = true;
    private Transform otherPlayerBox;

    public GameObject splash;
    GameObject[] particles;
    public UnityEvent celebrationEvents;



    void Start()
    {
        particles = new GameObject[4];
        startPos = transform.position;
        if (GetComponent<Animator>())
        {
            _animator = GetComponent<Animator>();
        }

        currentPos = transform.position;


        if (GetComponent<AIBehaviour>())
        {
            return;
        }
         otherPlayerBox = GameObject.FindGameObjectWithTag((tag.Equals("Box")) ? "Box2" : "Box").transform;
    }


    void Update()
    {
        float distOtherPlayer =3;

        if (otherPlayerBox != null)
        {
            distOtherPlayer = Vector3.Distance(otherPlayerBox.position, transform.position);
        }

        if (distOtherPlayer < 1f && canLose)
        {
            ActiveExplotion();
            gameObject.SetActive(false);
        }
        else
        {
            OnPlayerMove?.Invoke();
        }

        if (falling)
        {
            transform.Translate(0,-10*Time.deltaTime,0);
        }

    }
   
    
    public void Move(Vector3 finalPos)
    {
        splash.SetActive(false);
       if (canMove && !IsArrived)
       {
            //IsArrived = false;

            if (finalPos.y > Mathf.RoundToInt(transform.position.y))
            {
                Sequence _sequ = DOTween.Sequence();
                //   _sequ.Append(transform.DOJump(finalPos+ Vector3.up/2, jumpForce, 1, timeAnimation/3, false).SetEase(moveJumpCurveBegin));
                //_sequ.Append(transform.DOMove(finalPos, timeAnimation / 3, false).SetEase(moveJumpCurveEnd));

                _sequ.Append(transform.DOMove(finalPos + Vector3.up * jumpForce, timeAnimation / 3, false).SetEase(moveStepCurveBegin));
                _sequ.Append(transform.DOMove(finalPos, timeAnimation / 4, false).SetEase(moveStepCurveEnd));

            }
            else if (Mathf.RoundToInt(finalPos.y) == Mathf.RoundToInt(transform.position.y))
            {

                Sequence _sequ =  DOTween.Sequence();
                //_sequ.Append(transform.DOJump(finalPos+Vector3.up/2, jumpForce, 1, timeAnimation/3, false).SetEase(moveJumpCurveBegin));
                //_sequ.Append(transform.DOMove(finalPos, timeAnimation / 3, false).SetEase(moveJumpCurveEnd));

                 _sequ.Append(transform.DOMove(finalPos + Vector3.up * jumpForce, timeAnimation/3, false).SetEase(moveStepCurveBegin));
                 _sequ.Append(transform.DOMove(finalPos, timeAnimation / 4, false).SetEase(moveStepCurveEnd));
            }
            else
            {
               
                Sequence _sequ = DOTween.Sequence();
               // _sequ.Append(transform.DOJump(finalPos+ Vector3.up/2, jumpForce, 1, timeAnimation/3, false).SetEase(moveJumpCurveBegin));
               // _sequ.Append(transform.DOMove(finalPos, timeAnimation / 3, false).SetEase(moveJumpCurveEnd));

                _sequ.Append(transform.DOMove(finalPos + Vector3.up * jumpForce, timeAnimation / 3, false).SetEase(moveStepCurveBegin));
                _sequ.Append(transform.DOMove(finalPos, timeAnimation / 4, false).SetEase(moveStepCurveEnd));

            }

            _animator.SetTrigger("Jump");
            Invoke("ArriveNextDir", timeAnimation/1.5f);
            canMove = false;
            Invoke("SetCanMove",(timeAnimation >0) ? timeAnimation/3.5f :0.1f);
       }
    }

    public void Fall(Vector3 finalPos)
    {
        if (canMove && !IsArrived)
        {
            IsArrived = false;
            transform.DOJump(finalPos, jumpForce, 1, timeAnimation, false).SetEase(moveStepCurveEnd);
            canMove = false;
            falling = true;
        }

    }

    public void ActiveExplotion()
    {
        canMove = false;
        explotion.transform.SetParent(null);
        explotion.transform.localScale = Vector3.one;
        explotion.SetActive(true);
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

    }

    private void SetCanMove()
    {
        canMove = true;
    }

    public void SetCanMove( bool _canIMove)
    {
        canMove = _canIMove;
    }

    public void SetPathCompleted()
    {
        _animator.SetBool("Celebration",true);
        celebrationEvents?.Invoke();
        IsArrived = true;
    }

    public void SetNextQuad(ColorChanging newQuadDir)
    {
        if (newQuadDir == null) { return; }
        if (nextQuad != null)
        {
            previousQuad = nextQuad;
        }

        nextQuad = newQuadDir;

        if (previousQuad != null)
        {
            previousQuad.isSomethingOverme = null;

        }

        if (nextQuad != null)
        {
            nextQuad.isSomethingOverme = gameObject;
        }

    }

    public void SetPreviousQuad()
    {
        if (previousQuad != null)
        {
            nextQuad = previousQuad;
        }
    }

   private void ArriveNextDir()
    {
        if (nextQuad != null)
        {
            nextQuad.ChangeColor(tag);
        }
        ActiveSplash();
        _animator.SetTrigger("Fall");
    }

    void ActiveSplash()
    {
        for (int i = 0; i < particles.Length; i++) 
        {
            if (particles[i] == null)
            {
                particles[i] = Instantiate(splash, transform.position + new Vector3(0, -0.5f, 0), transform.rotation);
                particles[i].SetActive(true);
                break;
            }
            else
            {
                if (!particles[i].activeInHierarchy)
                {
                    particles[i].transform.position = transform.position + new Vector3(0, -0.5f, 0);
                    particles[i].SetActive(true);
                }
                else
                {
                    particles[i] = Instantiate(splash, transform.position + new Vector3(0, -0.5f, 0), transform.rotation);
                    particles[i].SetActive(true);

                }
                break;
            }
        }

    }

}
