﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explotion : MonoBehaviour
{
    GameObject[] childs;
    Vector3[] startPositions;
    Quaternion[] startRotations;


    private void Start()
    {
        childs = new GameObject[transform.childCount];
        startPositions = new Vector3[transform.childCount];
        startRotations = new Quaternion[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            childs[i] = transform.GetChild(i).gameObject;
            startPositions[i] = childs[i].transform.position;
            startRotations[i] = childs[i].transform.rotation;

        }
    }

    private void OnEnable()
    {
        childs = new GameObject[transform.childCount];
        startPositions = new Vector3[transform.childCount];
        startRotations = new Quaternion[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            childs[i] = transform.GetChild(i).gameObject;
            startPositions[i] = childs[i].transform.position;
            startRotations[i] = childs[i].transform.rotation;

        }

        childs[0].GetComponent<BoxCollider>().isTrigger = true;
    }

    private void OnDisable()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            childs[i].transform.position = startPositions[i];
            childs[i].transform.rotation = startRotations[i];
            childs[i].GetComponent < Rigidbody >().velocity = Vector3.zero;

        }
    }
}
