﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : MonoBehaviour
{
    [SerializeField] private TriggerDetector triggerDetect;
    private GridManager grid;
    public event System.Action OnCatched;

    private bool somethingInsidePreviousEnable;

    private void Start()
    {
        triggerDetect.OnTriggerEntered += Collected;
        grid = GameObject.FindWithTag("GameController").GetComponent<GridManager>();
        grid.GetComponent<ScoringSystem>().OnRestartLevel += RestoreColliders;

    }

    private void Collected(Collider other)
    {
        OnCatched?.Invoke();
       // GetComponent<Collider>().enabled = false;
      //  GetComponent<Renderer>().enabled = false;
      gameObject.SetActive(false);

    }

    public void RestoreColliders()
    {

        //GetComponent<Collider>().enabled = true;
        // GetComponent<Renderer>().enabled = true;
        gameObject.SetActive(true);
    }


}
