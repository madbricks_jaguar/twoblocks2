﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class CameraSwipeMove : MonoBehaviour
{
    [HideInInspector]
    public Vector3 targetPosition;

    private Vector3 targetInput;
    private Vector3 startInput;

    public float upperLimit { get; set; }
    public float lowerLimit { get; set; }

    public bool useElasticLimit { get; set; }
    public float timeLoadNextScenary { get; set;}

    public event System.Action<float> OnLimited;


    void Start()
    {
        InputManager.Instance.OnTapped += StopMovement;
        upperLimit = transform.position.z + 5000;
        lowerLimit = transform.position.z - 5000;

        targetPosition = transform.position;
        
    }

    private void Update()
    {
        Vector3 velocityZro = Vector3.zero;

        if (Input.GetMouseButtonDown(0))
        {
            startInput.y = Input.mousePosition.y;
            useElasticLimit = false;
        }

        if (Input.GetMouseButton(0))
        {
            DraggCamera();
        }

        if (Input.GetMouseButtonUp(0))
        {
            timeLoadNextScenary = 0;
            OnLimited?.Invoke(timeLoadNextScenary);
            ElasticLimit();
        }

        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocityZro, 0.09f);

        if (!useElasticLimit)
        {
            LimitZAxis();
        }

        if (Vector3.Distance(transform.position, targetPosition) < 0.5f)
        {
            useElasticLimit = false;
        }

    }

    private void ElasticLimit()
    {
        if (transform.position.z > upperLimit)
        {
            targetPosition = new Vector3(transform.position.x, transform.position.y, upperLimit);
            useElasticLimit = true;
            
        }

        if (transform.position.z < lowerLimit)
        {
            targetPosition = new Vector3(transform.position.x, transform.position.y, lowerLimit);
            useElasticLimit = true;
        }
    }

    private void DraggCamera()
    {
        targetInput.y = Input.mousePosition.y;
        targetPosition = transform.position;
        targetPosition.z = transform.position.z - (targetInput.y - startInput.y) / 60;

    }

    private void LimitZAxis()
    {
        if (transform.position.z > upperLimit +5)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, upperLimit+5);
            timeLoadNextScenary += 0.03f;
            OnLimited?.Invoke(timeLoadNextScenary);
        }

        if (transform.position.z < lowerLimit-5)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y, lowerLimit-5);
        }
    }

    private void StopMovement()
    {
        targetPosition = transform.position;
    }
}
